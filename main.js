import "./style.css";
import myTodoAppManager from './src/services/Todo-app';


import Header from './src/templates/header';
import Layout from './src/templates/layout';
import ProjectsList from './src/templates/projectsList';
import ProjectDetails from './src/templates/projectDetails';

let sitePrefs = {
  theme: 'dark',
}
if(localStorage.getItem("site-prefs")){
  sitePrefs = {...sitePrefs, ...JSON.parse(localStorage.getItem("site-prefs"))};
}
document.documentElement.dataset.theme=sitePrefs.theme;
document.documentElement.style.colorScheme=sitePrefs.theme;


const handleAddProject = (title)=>{
  myTodoAppManager.projects.add(title)
}
const handleRemoveProject = (projectId)=>{
  myTodoAppManager.projects.remove(projectId);
}
const handleUpdateProject = (projectId, title)=>{
  myTodoAppManager.projects.update(projectId, title);
}

const handleAddTodo = (todoObj)=>{
  myTodoAppManager.todos.add(todoObj);
}
const handleRemoveTodo = (todoId) =>{
  myTodoAppManager.todos.remove(todoId);
}
const handleUpdateTodo = (todoId, data) => {
  myTodoAppManager.todos.update(todoId, data);
}

const handleSelectProject = (projectId)=>{
  myTodoAppManager.currentProject = projectId;
  displayProject();
}

const displayProjects = ()=>{
  console.log(Layout);
  const collection = myTodoAppManager.projects.all();
  const sidebar = Layout.querySelector(".sidebar");
  while(sidebar.firstChild) sidebar.firstChild.remove();
  sidebar.append(ProjectsList({
    collection,
    selectProject: handleSelectProject,
    addProject: handleAddProject,
    removeProject: handleRemoveProject,
    updateProject: handleUpdateProject
  }))  
  displayProject();
}

const displayProject = () => {
  const project = myTodoAppManager.projects.byId(myTodoAppManager.currentProject);
  const content = Layout.querySelector('.main-content');
  while(content.firstChild) content.firstChild.remove();

  content.append(ProjectDetails({
    project,
    addTodo: handleAddTodo,
    removeTodo: handleRemoveTodo,
    updateTodo: handleUpdateTodo
  }))

}

Header.querySelector("h1").textContent = myTodoAppManager.title;
displayProjects();

myTodoAppManager.projects.subscribe(displayProjects);
myTodoAppManager.todos.subscribe(displayProjects);
myTodoAppManager.currentProjectListener(displayProjects)

document.querySelector('#app').append(Header, Layout);



Header.querySelector(".light-switch").addEventListener("click", (e)=>{
  if(document.documentElement.dataset.theme==='light'){
    document.documentElement.dataset.theme='dark';
    document.documentElement.style.colorScheme='dark';
    sitePrefs={...sitePrefs, theme: 'dark'};
  } else {
    document.documentElement.dataset.theme='light';
    document.documentElement.style.colorScheme='light';
    sitePrefs={...sitePrefs, theme: 'light'};
  }
  localStorage.setItem('site-prefs', JSON.stringify(sitePrefs));
  e.currentTarget.querySelector("i").classList.toggle("bi-brightness-high");
  e.currentTarget.querySelector("i").classList.toggle("bi-moon-stars");
})