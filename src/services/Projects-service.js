import { store } from './store';
import * as Todo from './Todos-service';

store.setTablesSchema({
  projects: {
    title: {type: 'string', default: 'Default project'}
  },
  todos: {
    pid: {type: 'string'},
    title: {type: 'string', default: 'Default title'},
    description: {type: 'string'},
    due: { type: 'string'},
    priority: {type: 'string', default: 'normal'}
  }
});

export const add = (title) => {
  const id = crypto?.randomUUID();
  store.setRow('projects', id, {title});

  return {[id]: {title} };
}

export const update = (projectId, title)=>
  store
    .setRow('projects', projectId, {title})
    .getRow('projects', projectId);

export const remove = (projectId) => {
  Todo.idsByProject(projectId).forEach(Todo.remove);
  store.delRow('projects', projectId);
}

export const all = () => store.getRowIds('projects').map(byId)

export const byId = (projectId) =>({
    [projectId]: store.getRow('projects', projectId),
    todos: Todo.byProject(projectId),
  });

export const subscribe = (fn) => store.addTableListener('projects', fn)
export const unsubscribe = (listenerId) => store.delListener(listenerId);