import { createStore, createIndexes } from "tinybase";
import { createLocalPersister } from "tinybase";

export const store = createStore();
export const indexes = createIndexes(store);

export const storage = createLocalPersister(store, 'TodoAppStore');

window.myStoryStoryStore = store;