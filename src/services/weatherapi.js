// Huge shoutout to https://open-meteo.com

const weatherUrl = (latitude, longitude)=> `https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&hourly=temperature_2m,relativehumidity_2m,apparent_temperature,precipitation,weathercode,windspeed_10m,windgusts_10m&current_weather=true`;
const geoSearchUrl = (location)=>`https://geocoding-api.open-meteo.com/v1/search?name=${location}`;

const weatherAPI = (()=>{
  const getLocation = async(string) => {
    try {
      const response = await fetch(geoSearchUrl(string));
      const result = response.json();
      return result;
    } catch(err){
      console.log(err);
    }
  }

  const getCurrentWeather = async(latitude, longitude) => {
    try {
      const response = await fetch(weatherUrl(latitude, longitude));
      const result = response.json();
      return result;
    } catch(err){
      console.log(err);
    }

  }

  return {
    getLocation,
    getCurrentWeather
  }
})();

export default weatherAPI;