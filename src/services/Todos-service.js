import { store, indexes } from "./store";

// type TodoProps = {
//   title: string
//   description?: string
//   due: string | number
//   priority: string
//   pid: string
// }

indexes.setIndexDefinition(
  'byProjectId',
  'todos',
  'pid'
); 

export const add = (todoObject) => {
  const id = crypto?.randomUUID();
  store.setRow('todos', id, todoObject);

  return {[id]: store.getRow('todos', id) };
}

export const update = (todoId, todoObject)=>
  store
    .setPartialRow('todos', todoId, todoObject)
    .getRow('todos', todoId);

export const remove = (todoId) => 
  store.delRow('todos', todoId);

export const byId = (todoId) => store.getRow('todos', todoId);

export const idsByProject = (projectId) =>
  indexes.getSliceRowIds('byProjectId', projectId);

export const byProject = (projectId) => 
  indexes
    .getSliceRowIds('byProjectId', projectId)
    .map(id =>
      ({ [id]: byId(id) })
    );

export const subscribe = (fn) => store.addTableListener('todos', fn)
export const unsubscribe = (listenerId) => store.delListener(listenerId);