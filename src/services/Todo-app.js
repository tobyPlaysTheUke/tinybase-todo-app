import { store, storage } from './store';

import * as Todos from './Todos-service';
import * as Projects from './Projects-service';

if(
  JSON.parse(localStorage.getItem("TodoAppStore"))
    .some( table=>Object.keys(table).includes('projects'))){
  await storage.load();
  store.setValue('currentProject', store.getRowIds('projects')[0]);
  storage.startAutoSave();

} else {
  const projectTitles = ['Personal','Work: team', 'Work: solo', 'Family']

  projectTitles.forEach(Projects.add)

  Todos.add({title: 'Make food', description: 'Breafast!', priority: 'high', due: new Date().toLocaleDateString(), pid: store.getRowIds('projects')[0]});
  store.setValue('currentProject', store.getRowIds('projects')[0]);
  storage.startAutoSave();
  
}


const myTodoAppManager = ({
  title: 'My Todo App!',
  projects: {
    ...Projects
  },
  todos: {
    ...Todos
  },
  get currentProject(){ return store.getValue('currentProject') },
  set currentProject(value){ store.setValue('currentProject', value)},
  currentProjectListener: (fn)=>{ store.addValueListener('currentProject', fn) }
});

export default myTodoAppManager;