import toHtml from "../util/toHtml";

const Header = toHtml(`<header class='container flex justify-between px-3 py-2'>
  <h1 class='inline-block text-3xl font-black text-indigo-800 p-6'>Admin Panel</h1>
  <div class='header-controls'>
  <div class='light-switch btn btn-ghost text-3xl font-black text-secondary'><i class="bi-brightness-high"></i></div>
  <div class="avatar avatar-ring-secondary avatar-online">
  <div class="dropdown-container">
    <div class="dropdown dropdown-open">
      <label
        class="btn btn-ghost flex cursor-pointer px-0 hover:bg-inherit"
        tabindex="0"
      >
        <img
          src="https://i.pravatar.cc/150"
          alt="avatar"
        />
      </label>
      <div class="dropdown-menu dropdown-menu-bottom-left">
        <div class="dropdown-item">
          <p class="text-sm leading-5 text-content1">Signed in as</p>
          <p class="truncate text-sm leading-5 text-content2">
            <strong>tom@example</strong>
          </p>
        </div>
        <div class="">
          <a
            tabindex="-1"
            class="dropdown-item flex w-full justify-between text-left text-sm leading-5 text-content2"
            role="menuitem"
          >
            <span>Account settings</span>
          </a>
          <a
            tabindex="-1"
            class="dropdown-item flex w-full justify-between text-left text-sm leading-5 text-content2"
            role="menuitem"
          >
            <span>Profile</span>
          </a>
          <a
            tabindex="-1"
            class="dropdown-item flex w-full justify-between text-left text-sm leading-5 text-content2"
            role="menuitem"
          >
            <span>Subscriptions</span>
          </a>
        </div>
        <div>
          <a
            class="dropdown-item flex w-full justify-between text-left text-sm leading-5 text-content2 avatar"
            role="menuitem"
          >
            <span>Log Out</span>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
  </div>
</header>`);

export default Header;