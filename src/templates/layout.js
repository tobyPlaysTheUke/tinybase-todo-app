import toHtml from "../util/toHtml"

const Layout = toHtml(`<main class='container flex'>
  <div>
    <input type='checkbox' id='drawer-left' class='drawer-toggle' />
    <label for='drawer-left' class='btn btn-primary'>
      <p>Open left</p>
    </label>
    <label class='overlay' for='drawer-left'></label>
    <div class='drawer'>
      <div class='drawer-content'>
        <label class='btn btn-circle btn-ghost absolute top-1 right-1' for='drawer-left'>X</label>
        <div class='sidebar mt-6'></div>
      </div>
    </div>
  </div>
  <div class='main-content ml-8'></div>
</main>`);

export default Layout;