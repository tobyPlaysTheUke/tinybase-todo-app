import timingController from "../util/timing";
import toHtml from '../util/toHtml';
import weatherAPI from "../services/weatherapi";

const throttle = timingController().throttle;


const updateLocations = (el) => async (value) => {
  const result = await weatherAPI.getLocation(value);
  while(el.firstChild) el.firstChild.remove();
  if(result.results) {
    el.append(...result.results?.map(
    location=>toHtml(`<option value="${location.name} ${location.admin1 || ''}"
       data-latitude="${location.latitude}"
       data-longitude="${location.longitude}">`)
  ))
    }
};

const Home = ()=>{
  const el = toHtml(`<div class='home-container'>
  <input list='locations' type='text' name='foo' placeholder='foo text' />
  <datalist id='locations'></datalist>
  <div class='foo-result'></div>
</div>`);

  el.querySelector('[name="foo"]').addEventListener(
    "input", 
    (e)=>{
      throttle(()=>updateLocations(el.querySelector("#locations"))(e.target.value), 500)
    },
    false
  )
  el.querySelector('[name="foo"]').addEventListener("input",
  async (e)=>{
    let options = [...el.querySelector("#locations").childNodes];
    let selection = options?.find(option=>option.value===e.target.value);
    const {latitude, longitude} = selection? selection.dataset : {latitude: undefined, longitude: undefined};
    if(latitude&&longitude){
      console.log( await(weatherAPI.getCurrentWeather(latitude, longitude)) );
    }
  })
  return el;
}

export default Home;